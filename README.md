# Rusty BST

> This project is based on [Assignment 6.2: Rusty Search Trees](https://cs242.stanford.edu/f19/assignments/assign6/#2-rusty-search-trees-40) of Stanford's [CS 242: Programming Languages](https://cs242.stanford.edu/f19/) course in Fall 2019.

This project implements a binary search tree in rust using the given template.