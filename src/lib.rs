use crate::BinaryTree::Node;
use std::fmt::{Debug, Display};
use std::{fmt, mem};

#[derive(PartialEq, Eq, Clone)]
pub enum BinaryTree<T> {
    Leaf,
    Node(T, Box<BinaryTree<T>>, Box<BinaryTree<T>>),
}

impl<T> BinaryTree<T> {
    fn new(t: T) -> BinaryTree<T> {
        Node(t, Box::new(BinaryTree::Leaf), Box::new(BinaryTree::Leaf))
    }
}

impl<T: Debug + Display + PartialOrd> BinaryTree<T> {
    pub fn len(&self) -> usize {
        fn dfs<T>(node: &BinaryTree<T>) -> usize {
            if let Node(_root, left, right) = node {
                return 1 + dfs(&left) + dfs(&right);
            }
            0
        }
        dfs(&self)
    }

    pub fn to_vec(&self) -> Vec<&T> {
        // Uses iterative inorder traversal
        let mut stack = vec![];
        let mut vec = vec![];
        let mut node = self;
        loop {
            while let Node(n, l, r) = node {
                stack.push(node);
                node = l;
            }
            let popped_node = stack.pop();
            match popped_node {
                Some(n) => node = n,
                None => break,
            }

            if let Node(n, l, r) = node {
                vec.push(n);
                node = r;
            }
        }
        vec
    }

    pub fn sorted(&self) -> bool {
        let mut stack = vec![];
        let node = self;
        stack.push((node, None, None));

        while !stack.is_empty() {
            match stack.pop() {
                Some((node, left_limit, right_limit)) => {
                    if let Node(n, l, r) = node {
                        if let Some(ll) = left_limit {
                            if !(n > ll) {
                                return false;
                            }
                        }
                        if let Some(rr) = right_limit {
                            if !(n < rr) {
                                return false;
                            }
                        }
                        stack.push((l, left_limit, Some(n)));
                        stack.push((r, Some(n), right_limit));
                    }
                }
                None => (),
            }
        }
        return true;
    }

    pub fn insert(&mut self, t: T) {
        let mut node = self;

        if let BinaryTree::Leaf = node {
            node = &mut Node(t, Box::new(BinaryTree::Leaf), Box::new(BinaryTree::Leaf));
            return;
        }

        while let Node(val, left, right) = node {
            if t < *val {
                if let BinaryTree::Leaf = **left {
                    mem::replace(left, Box::new(BinaryTree::new(t)));
                    break;
                }
                node = left;
            } else {
                if let BinaryTree::Leaf = **right {
                    mem::replace(right, Box::new(BinaryTree::new(t)));
                    break;
                }
                node = right;
            }
        }
    }

    pub fn search(&self, query: &T) -> Option<&T> {
        let mut res = None;
        let mut node = self;
        while let Node(val, left, right) = node {
            if query == val {
                return Some(val);
            }
            if val < query {
                node = right;
            } else {
                res = Some(val);
                node = left;
            }
        }
        res
    }

    pub fn rebalance(&mut self) {
        unimplemented!()
    }

    // Adapted from https://github.com/bpressure/ascii_tree
    fn fmt_levels(&self, f: &mut fmt::Formatter<'_>, level: Vec<usize>) -> fmt::Result {
        use BinaryTree::*;
        const EMPTY: &str = "   ";
        const EDGE: &str = " └─";
        const PIPE: &str = " │ ";
        const BRANCH: &str = " ├─";

        let maxpos = level.len();
        let mut second_line = String::new();
        for (pos, l) in level.iter().enumerate() {
            let last_row = pos == maxpos - 1;
            if *l == 1 {
                if !last_row {
                    write!(f, "{}", EMPTY)?
                } else {
                    write!(f, "{}", EDGE)?
                }
                second_line.push_str(EMPTY);
            } else {
                if !last_row {
                    write!(f, "{}", PIPE)?
                } else {
                    write!(f, "{}", BRANCH)?
                }
                second_line.push_str(PIPE);
            }
        }

        match self {
            Node(s, l, r) => {
                let mut d = 2;
                write!(f, " {}\n", s)?;
                for t in &[l, r] {
                    let mut lnext = level.clone();
                    lnext.push(d);
                    d -= 1;
                    t.fmt_levels(f, lnext)?;
                }
            }
            Leaf => write!(f, "\n")?,
        }
        Ok(())
    }
}

impl<T: Debug + Display + PartialOrd> fmt::Debug for BinaryTree<T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.fmt_levels(f, vec![])
    }
}

#[cfg(test)]
mod test {
    use super::BinaryTree::*;
    use crate::BinaryTree;
    use lazy_static::lazy_static;

    lazy_static! {
        static ref TEST_TREE: BinaryTree<&'static str> = {
            Node(
                "B",
                Box::new(Node("A", Box::new(Leaf), Box::new(Leaf))),
                Box::new(Node("C", Box::new(Leaf), Box::new(Leaf))),
            )
        };
    }

    lazy_static! {
        static ref EMPTY_TREE: BinaryTree<&'static str> = Leaf;
    }

    lazy_static! {
        static ref LOPSIDED_TREE: BinaryTree<&'static str> = {
            Node(
                "B",
                Box::new(Node("A", Box::new(Leaf), Box::new(Leaf))),
                Box::new(Leaf),
            )
        };
    }

    #[test]
    fn len_test_lopsided() {
        assert_eq!(LOPSIDED_TREE.len(), 2)
    }

    #[test]
    fn len_test_empty() {
        assert_eq!(EMPTY_TREE.len(), 0)
    }

    #[test]
    fn len_test() {
        assert_eq!(TEST_TREE.len(), 3);
    }

    #[test]
    fn to_vec_test() {
        assert_eq!(TEST_TREE.to_vec(), vec![&"A", &"B", &"C"]);
    }

    #[test]
    fn sorted_test() {
        let mut t = TEST_TREE.clone();
        assert!(t.sorted());

        t = Node("D", Box::new(Leaf), Box::new(t));
        assert!(!t.sorted());
    }

    #[test]
    fn insertion_test() {
        let mut t = TEST_TREE.clone();
        t.insert("E");
        assert!(t.sorted());
    }

    #[test]
    fn search_test() {
        let mut t = TEST_TREE.clone();
        t.insert("E");
        assert!(t.search(&"D") == Some(&"E"));
        assert!(t.search(&"C") == Some(&"C"));
        assert!(t.search(&"F") == None);
    }

    #[test]
    fn rebalance1_test() {
        let mut t = Node(
            "D",
            Box::new(Node(
                "B",
                Box::new(Node("A", Box::new(Leaf), Box::new(Leaf))),
                Box::new(Node("C", Box::new(Leaf), Box::new(Leaf))),
            )),
            Box::new(Node("E", Box::new(Leaf), Box::new(Leaf))),
        );

        let t2 = Node(
            "C",
            Box::new(Node(
                "B",
                Box::new(Node("A", Box::new(Leaf), Box::new(Leaf))),
                Box::new(Leaf),
            )),
            Box::new(Node(
                "D",
                Box::new(Leaf),
                Box::new(Node("E", Box::new(Leaf), Box::new(Leaf))),
            )),
        );

        t.rebalance();
        assert_eq!(t, t2);
    }

    #[test]
    fn rebalance2_test() {
        let mut t = Node(
            "A",
            Box::new(Leaf),
            Box::new(Node(
                "B",
                Box::new(Leaf),
                Box::new(Node(
                    "C",
                    Box::new(Leaf),
                    Box::new(Node("D", Box::new(Leaf), Box::new(Leaf))),
                )),
            )),
        );

        let t2 = Node(
            "B",
            Box::new(Node("A", Box::new(Leaf), Box::new(Leaf))),
            Box::new(Node(
                "C",
                Box::new(Leaf),
                Box::new(Node("D", Box::new(Leaf), Box::new(Leaf))),
            )),
        );

        t.rebalance();
        assert_eq!(t, t2);
    }

    #[test]
    fn rebalance3_test() {
        let mut t = Node(
            "E",
            Box::new(Node(
                "B",
                Box::new(Leaf),
                Box::new(Node(
                    "D",
                    Box::new(Node("C", Box::new(Leaf), Box::new(Leaf))),
                    Box::new(Leaf),
                )),
            )),
            Box::new(Node("F", Box::new(Leaf), Box::new(Leaf))),
        );

        let t2 = Node(
            "D",
            Box::new(Node(
                "B",
                Box::new(Leaf),
                Box::new(Node("C", Box::new(Leaf), Box::new(Leaf))),
            )),
            Box::new(Node(
                "E",
                Box::new(Leaf),
                Box::new(Node("F", Box::new(Leaf), Box::new(Leaf))),
            )),
        );

        t.rebalance();
        assert_eq!(t, t2);
    }
}
